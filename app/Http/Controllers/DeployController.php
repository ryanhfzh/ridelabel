<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeployController extends Controller
{
    public function deploy()
    {
        $exec = [];
        $exec[] = shell_exec('git pull origin master 2>&1');
        return var_dump($exec);
    }
    public function deploypush()
    {
        $exec = [];
        $exec[] = shell_exec('git add .');
        $exec[] = shell_exec('git commit -m "."');
        $exec[] = shell_exec('git push origin master');
        return var_dump($exec);
    }
    public function check()
    {
        $exec = [];
        //$exec[] = shell_exec('ls');
        $exec[] = shell_exec('composer install');
        $exec[] = shell_exec('cd .. & php artisan migrate  --seed');
        return var_dump($exec);
    }
}
