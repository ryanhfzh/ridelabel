<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Product;
use App\Categories;

class KategoriControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_kategori()
     {
         $user = factory(User::class)->create([
             'name' => $this->faker->name,
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $categories = factory(Categories::class)->create();
         $product = factory(Product::class)->create();
         $response = $this->actingAs($user)->call('GET',route('user.kategori',['id'=>1]));
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
}
