<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Province;
use App\City;
use App\Alamat;

class AlamatControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_visit_alamat()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $city = factory(City::class)->create();
         $alamat = factory(Alamat::class)->create([
             'cities_id' => 1,
             'user_id' => 1,
             'detail' => "Bandung"
         ]);
         $response = $this->actingAs($user)->get('/alamat');
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_alamat_empty()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $city = factory(City::class)->create();
         $alamat = factory(Alamat::class)->create([
             'cities_id' => 1,
             'user_id' => 2,
             'detail' => "Bandung"
         ]);
         $response = $this->actingAs($user)->get('/alamat');
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_alamat_getCity()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $province = factory(City::class)->create();
         $response = $this->actingAs($user)->call('GET',route('user.alamat.getCity',['id'=>1]));
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_alamat_ubah()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $response = $this->actingAs($user)->call('GET',route('user.alamat.ubah',['id'=>1]));
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_alamat_update()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $city = factory(City::class)->create();
         $alamat = factory(Alamat::class)->create([
             'cities_id' => 1,
             'user_id' => 1,
             'detail' => "Bandung"
         ]);
         $response = $this->actingAs($user)->call('POST',route('user.alamat.update',['id'=>1]), ['cities_id'=>1,'detail'=>'detail']);
         // $response->withExceptionHandling();
         $response->assertStatus(302);
         $response->assertRedirect(route('user.alamat'));
     }
     public function test_visit_alamat_simpan()
     {
         $user = factory(User::class)->create([
             'name' => 'hahha',
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
         $province = factory(Province::class)->create();
         $city = factory(City::class)->create();
         $alamat = factory(Alamat::class)->create([
             'cities_id' => 1,
             'user_id' => 1,
             'detail' => "Bandung"
         ]);
         $response = $this->actingAs($user)->call('POST',route('user.alamat.simpan'), ['cities_id'=>1,'detail'=>'detail']);
         // $response->withExceptionHandling();
         $response->assertStatus(302);
         $response->assertRedirect(route('user.alamat'));
     }
}
