<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class PelangganControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/admin/pelanggan');

        $response->assertStatus(200);
    }
}
