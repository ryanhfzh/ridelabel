<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PengaturanControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(\App\User::class)->create([
            'name' => $this->faker->name,
            'email' => 'ha@ha.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
            'role' => 'admin'
        ]);

        factory(\App\Alamat::class)->create(['user_id' => $this->user->id]);
        factory(\App\City::class)->create();
        factory(\App\Province::class,3)->create();
    }

    public function test_show_alamat_page()
    {
        factory(\App\Alamattoko::class)->create();
        $response = $this->actingAs($this->user)->get(route('admin.pengaturan.alamat'));

        $response->assertStatus(200);
    }

    public function test_show_alamat_page_with_data_provinces_only()
    {
        $response = $this->actingAs($this->user)->get(route('admin.pengaturan.alamat'));

        $response->assertStatus(200);
    }

    public function test_show_ubah_alamat_page()
    {
        $response = $this->actingAs($this->user)->get(route('admin.pengaturan.ubahalamat', ['id' => 1]));

        $response->assertStatus(200);
    }

    public function test_get_city()
    {
        $response = $this->actingAs($this->user)->get(route('admin.pengaturan.getCity', ['id' => 1]));

        $response->assertStatus(200);
    }

    public function test_simpan_alamat()
    {
        $response = $this->actingAs($this->user)->post(route('admin.pengaturan.simpanalamat'), [
            'cities_id' => 1,
            'detail' => 'Bandung'
        ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.pengaturan.alamat'));
    }

    public function test_update_alamat()
    {
        $alamatToko = factory(\App\Alamattoko::class)->create();
        $response = $this->actingAs($this->user)->post(route('admin.pengaturan.updatealamat', ['id' => 1]), [
            'cities_id' => $alamatToko,
            'detail' => 'Bandung'
        ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.pengaturan.alamat'));
    }
}
