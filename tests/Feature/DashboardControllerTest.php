<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class DashboardControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit_dashboard()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/admin');
        // $response->withExceptionHandling();
        $response->assertStatus(200);
    }
}
