<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Categories;

class CategoriesControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_visit_categories()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/categories');
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_categories_tambah()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/categories/tambah');
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_categories_store()
     {
         $user = factory(User::class)->create();
         $categories = factory(Categories::class)->create();
         $response = $this->actingAs($user)->call('POST',route('admin.categories.store'),['name'=>$this->faker->name]);
         // $response->withExceptionHandling();
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.categories'));
     }
     public function test_visit_categories_edit()
     {
         $user = factory(User::class)->create();
         $categories = factory(Categories::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.categories.edit',['id'=>1]));
         // $response->withExceptionHandling();
         $response->assertStatus(200);
     }
     public function test_visit_categories_update()
     {
         $user = factory(User::class)->create();
         $categories = factory(Categories::class)->create();
         $response = $this->actingAs($user)->call('POST',route('admin.categories.update',['id'=>1]),['name'=>$this->faker->name]);
         // $response->withExceptionHandling();
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.categories'));
     }
     public function test_visit_categories_delete()
     {
         $user = factory(User::class)->create();
         $categories = factory(Categories::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.categories.delete',['id'=>1]));
         // $response->withExceptionHandling();
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.categories'));
     }
}
