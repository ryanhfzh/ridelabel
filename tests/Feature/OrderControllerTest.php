<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\User;
use App\Keranjang;
use App\Order;
use App\Product;
use App\Detailorder;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     private $user;

    public function setUp(): void{
        parent::setUp();

        $this->user = factory(User::class)->create([
             'name' => $this->faker->name,
             'email' => 'ha@ha.com',
             'email_verified_at' => now(),
             'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
             'role' => 'customer'
         ]);
        factory(Keranjang::class)->create();
        factory(Detailorder::class)->create();
        factory(Order::class)->create([
            'invoice' => 'INV/011/007',
            'user_id' => 1,
            'status_order_id' => 2,
            'metode_pembayaran' => 'cod',
            'ongkir' => 0,
            'biaya_cod' => 0,
            'subtotal' => 0,
            'pesan' => 'ha',
            'no_hp' => '08762351837'
        ]);
        factory(Product::class)->create();
        factory(\App\Orderstatus::class, 2)->create();
        \Storage::fake('public/storage/buktibayar');
    }

    public function test_visit_order()
    {
        $response = $this->actingAs($this->user)->get('/order');

        $response->assertStatus(200);
    }

    public function test_visit_order_no_auth()
    {
        $response = $this->get('/order');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_visit_order_detail()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.order.detail',['id'=>1]));

        $response->assertStatus(200);
    }

    public function test_visit_order_diterima()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.order.pesananditerima',['id'=>1]));
        $response->assertStatus(302);
        $response->assertRedirect(route('user.order'));
    }

    public function test_visit_order_dibatalkan()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.order.pesanandibatalkan',['id'=>1]));
        $response->assertStatus(302);
        $response->assertRedirect(route('user.order'));
    }

    public function test_visit_order_pembayaran()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.order.pembayaran',['id'=>1]));
        $response->assertStatus(200);
    }

    public function test_visit_order_kirimbukti()
    {
        $response = $this->actingAs($this->user)->call('POST', route('user.order.kirimbukti', ['id'=>1]),[
            'bukti_pembayaran' => UploadedFile::fake()->image('bukti-bayar.jpg')
         ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.order'));
    }

    public function test_visit_order_store_cod()
    {
        $response = $this->actingAs($this->user)->call('POST',route('user.order.simpan'), [
            'invoice' => 'inv/12/001',
            'user_id' => 1,
            'metode_pembayaran' => 'cod',
            'ongkir' => 0,
            'subtotal' => 0,
            'pesan' => '',
            'no_hp' => '08251283647'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.order.sukses'));
    }

    public function test_visit_order_store()
    {
        $response = $this->actingAs($this->user)->call('POST',route('user.order.simpan'), [
            'invoice' => 'inv/12/001',
            'user_id' => 1,
            'metode_pembayaran' => 'Virtual Account',
            'ongkir' => 0,
            'subtotal' => 0,
            'pesan' => '',
            'no_hp' => '08251283647'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.order.sukses'));
    }

    public function test_visit_order_empty()
    {
        $response = $this->actingAs($this->user)->call('POST',route('user.order.simpan'), [
            'invoice' => 'INV/011/007',
            'user_id' => 1,
            'metode_pembayaran' => 'Virtual Account',
            'ongkir' => 0,
            'subtotal' => 0,
            'pesan' => '',
            'no_hp' => '08251283647'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.keranjang'));
    }

    public function test_visit_sukses_page()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.order.sukses'));
        $response->assertStatus(200);
    }
}
