<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Rekening;

class RekeningControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_visit_rekening()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/rekening');

         $response->assertStatus(200);
     }

     public function test_visit_rekening_tambah()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/rekening/tambah');

         $response->assertStatus(200);
     }

     public function test_visit_rekening_store()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->call('POST',route('admin.rekening.store'), ['bank_name'=>'MANDIRI','atas_nama'=>'PT RIDE LABEL','no_rekening'=>'32121343214']);
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.rekening'));
     }

     public function test_visit_rekening_edit()
     {
         $user = factory(User::class)->create();
         $rekening = factory(Rekening::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.rekening.edit',['id'=>1]));
         $response->assertStatus(200);
     }

     public function test_visit_rekening_update()
     {
         $user = factory(User::class)->create();
         $rekening = factory(Rekening::class)->create();
         $response = $this->actingAs($user)->call('POST',route('admin.rekening.update',['id'=>1]), ['bank_name'=>'MANDIRI','atas_nama'=>'PT RIDE LABEL','no_rekening'=>'32121343214']);
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.rekening'));
     }

     public function test_visit_rekening_delete()
     {
         $user = factory(User::class)->create();
         $rekening = factory(Rekening::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.rekening.delete',['id'=>1]));
         $response->assertStatus(302);
         $response->assertRedirect(route('admin.rekening'));
     }
}
