<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\User;
use App\Categories;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private $user;

    /**
     * set up for testing
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        factory(\App\Categories::class)->create();
        factory(\App\Product::class, 15)->create();
        \Storage::fake('public/storage/imageproduct');
        \Storage::fake('public/storage/arproduct');
    }

    public function test_visit_product()
    {
        $response = $this->actingAs($this->user)->get('/admin/product');

        $response->assertStatus(200);
    }

    public function test_visit_product_tambah()
    {
        $response = $this->actingAs($this->user)->get('/admin/product/tambah');

        $response->assertStatus(200);
    }

    public function test_visit_product_store()
    {
        $response = $this->actingAs($this->user)->post('/admin/product/store',[
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'price' => 1,
            'stok' => 10,
            'weigth' => 250,
            'categories_id' => 1,
            'image' => UploadedFile::fake()->image('product-photo.jpg'),
            'ar' => UploadedFile::fake()->image('product-ar.jpg'),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/admin/product');

    }

    public function test_visit_categories_edit()
    {
        $response = $this->actingAs($this->user)->get('/admin/product/edit/1');

        $response->assertStatus(200);
    }

    public function test_visit_categories_update()
    {
        $response = $this->actingAs($this->user)->post('/admin/product/update/1', [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'price' => 1,
            'stok' => 10,
            'weigth' => 250,
            'categories_id' => 1,
            'image' => UploadedFile::fake()->image('product-photo.jpg'),
            'ar' => UploadedFile::fake()->image('product-ar.jpg'),
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/admin/product');
    }

    public function test_visit_categories_delete()
    {
        $response = $this->actingAs($this->user)->get('/admin/product/delete/1');

        $response->assertStatus(302);
        $response->assertRedirect('/admin/product');
    }
}
