<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Order;
use App\Detailorder;
use App\Orderstatus;
use App\Product;

class TransaksiControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_visit()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi');

         $response->assertStatus(200);
     }
     public function test_visit_perludicek()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi/perludicek');

         $response->assertStatus(200);
     }
     public function test_visit_perludikirim()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi/perludikirim');

         $response->assertStatus(200);
     }
     public function test_visit_dikirim()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi/dikirim');

         $response->assertStatus(200);
     }
     public function test_visit_selesai()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi/selesai');

         $response->assertStatus(200);
     }
     public function test_visit_dibatalkan()
     {
         $user = factory(User::class)->create();
         $response = $this->actingAs($user)->get('/admin/transaksi/dibatalkan');

         $response->assertStatus(200);
     }
     public function test_visit_detail_order()
     {
         $user = factory(User::class)->create();
         $order = factory(Order::class)->create();
         $detailorder = factory(Detailorder::class)->create();
         $statusorder = factory(Orderstatus::class)->create();
         $product = factory(Product::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.transaksi.detail',['id'=>1]));

         $response->assertStatus(200);
     }
     public function test_input_resi()
     {
         $user = factory(User::class)->create();
         $order = factory(Order::class)->create();
         $response = $this->actingAs($user)->post('/admin/transaksi/inputresi/1', [
             'no_resi' => 'tsi291js'
         ]);

         $response->assertStatus(302);
         $response->assertRedirect(route('admin.transaksi.perludikirim'));
     }
     public function test_konfirmasi()
     {
         $user = factory(User::class)->create();
         $order = factory(Order::class)->create();
         $detailorder = factory(Detailorder::class)->create();
         $product = factory(Product::class)->create();
         $response = $this->actingAs($user)->call('GET',route('admin.transaksi.konfirmasi',['id'=>1]));

         $response->assertStatus(302);
         $response->assertRedirect(route('admin.transaksi.perludikirim'));
     }
}
