<?php

namespace Tests\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login()
    {
        $this->withExceptionHandling();
        $user = factory(User::class)->create([
            'name' => "hahaha",
            'email' => 'admin@ridelabel.com',
            'password' => '$2y$12$.cGze21UDbkUqAzpEbw8d.JB/mCmQFvKwPcCSVlKW2WWNtLGD17Hy ', // password
            'role' => 'customer'
        ]);
        $response = $this->post('/login', [
            'email' => 'admin@ridelabel.com',
            'password' => 'qwerty123',
            '_token' => csrf_token(),
        ]);
        $response->assertSessionHasErrors('email');
        $response->assertStatus(302);
    }
    public function test_visit_login()
    {
        $user = factory(User::class)->create([
            'name' => 'hahaha',
            'email' => 'admin@ridelabel.com',
            'password' => '$2y$12$.cGze21UDbkUqAzpEbw8d.JB/mCmQFvKwPcCSVlKW2WWNtLGD17Hy ', // password
            'role' => 'admin'
        ]);

        $response = $this->actingAs($user)
                        ->get('/login');

        $response->assertStatus(302);
    }
    public function test_logout()
    {
        $response = $this->post('/logout');

        $response->assertStatus(302);
    }
}
