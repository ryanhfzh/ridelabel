<?php

namespace Tests\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function test_visit_home()
    {
        $response = $this->get('/home');

        $response->assertStatus(200);
    }
    public function test_visit_tentang()
    {
        $response = $this->get('/tentang');

        $response->assertStatus(200);
    }
}
