<?php

namespace Tests\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\User;

class RegisterControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_stores_data(){
        //TODO: code inside here --Created by Kiddy

        $pass = Str::random(10);
        $datas = [
            //isi parameter sesuai kebutuhan request
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'password' => $pass,
            'password_validation' => $pass,
        ];
        // $this->withoutExceptionHandling();
        $response = $this->post('/register', $datas);
        //Tuntutan status 302, yang berarti redirect status code.
        // $response->assertSessionHasErrors('name');
        $response->assertStatus(302);

        //Tuntutan bahwa abis melakukan POST URL akan dialihkan ke URL product atau routenya adalah product.index
        $response->assertRedirect('/');
    }
    public function test_visit(){
        //TODO: code inside here --Created by Kiddy
        $response = $this->get('/register');
        $response->assertStatus(200);
    }
    public function test_wrong_data(){
        // $user = factory(User::class)->create();
        //TODO: code inside here --Created by Kiddy

        $pass = Str::random(5);
        $datas = [
            //isi parameter sesuai kebutuhan request
            'email' => $this->faker->unique()->email
        ];
        //Acting as berfungsi sebagai autentikasi, jika kita menghilangkannya maka akan error.
        $response = $this->post('/register', $datas);
        //Tuntutan status 302, yang berarti redirect status code.
        $response->assertStatus(302);

        //Tuntutan bahwa abis melakukan POST URL akan dialihkan ke URL product atau routenya adalah product.index
        $response->assertRedirect('/');
    }
}
