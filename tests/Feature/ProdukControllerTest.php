<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Product;

class ProdukControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit()
    {
        $response = $this->get('/produk');

        $response->assertStatus(200);
    }
    public function test_visit_cari()
    {
        $response = $this->get('/produk/cari');

        $response->assertStatus(200);
    }
    public function test_detail_visit()
    {
        $product = factory(Product::class)->create();
        $response = $this->get(route('user.produk.detail', ['id' => 1]));
        $response->assertStatus(200);
    }
    public function test_gambarar()
    {
        $product = factory(Product::class)->create();
        $response = $this->get(route('ar',['id' => 1]));
        $response->assertStatus(200);
    }
}
