<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Keranjang;

class KeranjangControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => $this->faker->name,
            'email' => 'ha@ha.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
            'role' => 'customer'
        ]);
        factory(Keranjang::class)->create();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit()
    {
        $response = $this->actingAs($this->user)->get('/keranjang');

        $response->assertStatus(200);
    }

    public function test_visit_keranjang_store()
    {
        $response = $this->actingAs($this->user)->call('POST',route('user.keranjang.simpan'), ['user_id'=>1,'products_id'=>1,'qty'=>10]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.keranjang'));
    }

    public function test_visit_keranjang_update()
    {
        $response = $this->actingAs($this->user)->call('POST',route('user.keranjang.update'), [
            'id' => [1],
            'qty' => [19]
        ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('user.keranjang.update'));
    }

    public function test_visit_keranjang_delete()
    {
        $response = $this->actingAs($this->user)->call('GET',route('user.keranjang.delete',['id'=>1]));
        $response->assertStatus(302);
        $response->assertRedirect(route('user.keranjang'));
    }
}
