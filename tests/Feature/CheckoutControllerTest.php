<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Product;
use App\Keranjang;
use App\Province;
use App\City;
use App\Alamat;
use App\Alamattoko;

class CheckoutControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_checkout()
    {
        $user = factory(User::class)->create([
            'name' => "hahaha",
            'email' => 'ha@ha.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
            'role' => 'customer'
        ]);
        $keranjang = factory(Keranjang::class)->create();
        $province = factory(Province::class)->create();
        $city = factory(City::class)->create();
        $product = factory(Product::class)->create();
        $alamat = factory(Alamat::class)->create(['user_id'=>1]);
        $alamattoko = factory(Alamattoko::class)->create();
        $this->withoutExceptionHandling();
        $response = $this->actingAs($user)->call('GET',route('user.checkout',['id'=>1]));

        $response->assertStatus(200);
        // $this->assertTrue(true);
    }
}
