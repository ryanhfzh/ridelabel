<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'invoice' => 'INV/011/007',
        'user_id' => 1,
        'status_order_id' => 1,
        'metode_pembayaran' => '',
        'ongkir' => 0,
        'biaya_cod' => 0,
        'subtotal' => 0,
        'pesan' => '',
        'no_hp' => ''
    ];
});
