<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Alamattoko;
use Faker\Generator as Faker;

$factory->define(Alamattoko::class, function (Faker $faker) {
    return [
        'city_id' => 1,
        'detail' => "Bandung"
    ];
});
