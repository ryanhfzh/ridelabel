<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Alamat;
use Faker\Generator as Faker;

$factory->define(Alamat::class, function (Faker $faker) {
    return [
        'cities_id' => 1,
        'detail' => "Bandung"
    ];
});
