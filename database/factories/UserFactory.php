<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'admin@ridelabel.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$ns1f5xMlbTmtoUXVAWom/eVgZka7QhzBDDk3oeT8L8DiV5h2DX5Ya', // password
        'remember_token' => Str::random(10),
        'role' => 'admin'
    ];
});
