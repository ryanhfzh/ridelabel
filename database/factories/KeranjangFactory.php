<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Keranjang;
use Faker\Generator as Faker;

$factory->define(Keranjang::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'products_id' => 1,
        'qty' => 10
    ];
});
