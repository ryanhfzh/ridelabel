<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Orderstatus;
use Faker\Generator as Faker;

$factory->define(Orderstatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
