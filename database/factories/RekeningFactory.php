<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rekening;
use Faker\Generator as Faker;

$factory->define(Rekening::class, function (Faker $faker) {
    return [
        'id' => 1,
        'bank_name' => 'BCA',
        'atas_nama' => 'PT. RIDE LABEL',
        'no_rekening' => '123123332211',
    ];
});
