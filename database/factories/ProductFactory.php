<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->name,
        'image' => 'asd',
        'price' => 150000, // password
        'weigth' => 250,
        'categories_id' => 1,
        'stok' => 10,
        'ar' => 'a'
    ];
});
