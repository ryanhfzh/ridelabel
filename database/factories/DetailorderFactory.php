<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Detailorder;
use Faker\Generator as Faker;

$factory->define(Detailorder::class, function (Faker $faker) {
    return [
        'order_id' => 1,
        'product_id' => 1,
        'qty' => 1
    ];
});
